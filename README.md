# openal-redist
Redistributable package of [OpenAL SDK](https://openal.org) (**v1.1, Windows**) binaries to be used in other repositories as submodule.